﻿using System;

namespace ReverseFileReader
{
    class Program
    {
        static void Main(string[] args)
        {
            var reverseFileReader = new ReverseReader();
            reverseFileReader.Read("testFile.txt","output.txt");
        }
    }
}
