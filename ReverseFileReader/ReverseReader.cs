﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace ReverseFileReader
{
    public class ReverseReader
    {
        public void Read(string inputFileName, string outputfileName)
        {

            var inputPath = Path.Combine(Environment.CurrentDirectory, $"Data/{inputFileName}");
            var outputPath = Path.Combine(Environment.CurrentDirectory, $"Data/{outputfileName}");

            if (!File.Exists(inputPath))
            {
                throw new FileNotFoundException();
            }

            var inputLines = File.ReadAllLines(inputPath);

            if (inputLines?.Length <=  0)
            {
                throw new NoContentException();
            }

            List<string> outputLines = new List<string>();
            for(int i = inputLines.Length -1; i >=0; i--)
            {
                var line = inputLines[i];
                var reveresedLine = Reverse(line);
                outputLines.Add(reveresedLine);
            }

            File.WriteAllLines(outputPath, outputLines);
        }

        private static string Reverse(string s)
        {
            char[] charArray = s.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }
    }
}
