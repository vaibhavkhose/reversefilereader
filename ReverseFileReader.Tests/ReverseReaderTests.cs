using System;
using Xunit;
using FluentAssertions;
using System.Security.Cryptography;
using System.IO;

namespace ReverseFileReader.Tests
{
    public class ReverseReaderTests
    {
        [Fact]
        public void read_should_throw_exception_if_file_not_exists()
        {
            string fileName = "invalidfile.txt";
            var reverseReader = new ReverseReader();

            Action act = () => reverseReader.Read(fileName, "output.txt");
            act.Should().Throw<FileNotFoundException>();
        }


        [Fact]
        public void read_should_throw_exception_if_no_content_in_file()
        {
            string fileName = "EmptyFile.txt";
            var reverseReader = new ReverseReader();

            Action act = () => reverseReader.Read(fileName, "output.txt");
            act.Should().Throw<NoContentException>();
        }

        [Theory]
        [InlineData("testInput1.txt", "testOutput1.txt")]
        [InlineData("testInput2.txt", "testOutput2.txt")]
        [InlineData("testInput3.txt", "testOutput3.txt")]
        public void read_should_reverse_file_successfully(string inputFileName, string expectedOutput)
        {
            var reverseReader = new ReverseReader();

            reverseReader.Read(inputFileName, expectedOutput);

            FilesAreEqual_Hash(expectedOutput).Should().BeTrue();
        }

        static bool FilesAreEqual_Hash(string outputFileName)
        {
            var firstFile = new FileInfo(Path.Combine(Environment.CurrentDirectory, $"Data/{outputFileName}"));
            var secondFile = new FileInfo(Path.Combine(Environment.CurrentDirectory, $"Data/ExpectedOutput/{outputFileName}"));
            byte[] firstHash = MD5.Create().ComputeHash(firstFile.OpenRead());
            byte[] secondHash = MD5.Create().ComputeHash(secondFile.OpenRead());

            for (int i = 0; i < firstHash.Length; i++)
            {
                if (firstHash[i] != secondHash[i])
                    return false;
            }
            return true;
        }
    }
}
